<?php
    $locale = "zh_CN";

    putenv("LANG=" . $locale); 
    setlocale(LC_ALL, $locale);
    
    $domain = "example";
    bindtextdomain($domain, "Locale"); 
    bind_textdomain_codeset($domain, 'UTF-8');
    textdomain($domain);
?>

<!doctype html>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="alternate" hreflang="en" href="indexen.html">
    <link rel="alternate" hreflang="cn" href="index.php">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>AJAX</title>
  </head>
  <body class="container">
    
    
    <nav class="navbar navbar-light bg-light">
  <a class="navbar-brand">Language</a>
  <form class="form-inline">
    <a href="indexen.html"><button class="btn btn-outline-success my-2 my-sm-0" type="button">English</button></a>
  </form>
</nav>
    
    <h1>AJAX</h1>
  
    <form class="needs-validation" novalidate id="form1">
      
        <div class="row">
          <div class="form-group col-md-2">
            <label for="make"><?php echo gettext('Make') ?></label>
            <input type="text" class="form-control" id="make" placeholder="enter a maker" value="maker" required name="make">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="form-group col-md-2">
            <label for="model"><?php echo gettext('Model') ?></label>
            <input type="text" class="form-control" id="model" placeholder="enter a model" value="model" required name="model">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-2">
            <label for="year"><?php echo gettext('Year') ?></label>
            <input type="number" class="form-control" id="year" placeholder="enter a year" value="1990" required name="year" min="0">
            <div class="invalid-feedback">
              A positive valid year is required.
            </div>
          </div>
          <div class="form-group col-md-2">
            <label for="mileage"><?php echo gettext('Mileage') ?></label>
            <input type="number" class="form-control" id="mileage" placeholder="enter a mileage" value="10" required name="mileage" min="0">
            <div class="invalid-feedback">
              A positive valid mileage is required.
            </div>
          </div>
        </div>

      <div class="form-group">
       <button type="submit" class="btn btn-primary"><?php echo gettext('Add Car') ?></button>
      </div>
    </form>
    <ul class="list-group">
    </ul>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="form-validation.js"></script>
    
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function () {//1
        'use strict';

        window.addEventListener('load', function () {//2
          var list = document.querySelector('ul');
          var form = document.querySelector('form');
          var make = document.querySelector('#make');
          make.focus();
          var model = document.querySelector('#model');
          var year = document.querySelector('#year');
          var mileage = document.querySelector('#mileage');

          function addItem( record ) {
            
            var listItem = document.createElement('li');
            var listText = document.createElement('span');
            var listBtn = document.createElement('button');
            listBtn.setAttribute( "class", "btn btn-danger");
            listItem.setAttribute( "class", "list-group-item d-flex justify-content-between align-items-center");
            listItem.appendChild(listText);
            listText.textContent = record.make + " " + record.model + " " + record.year + " " + record.mileage;
            listItem.appendChild(listBtn);
            listBtn.textContent = ' <?php echo gettext('Delete') ?> ' ;
            list.appendChild(listItem);
            listBtn.onclick = function(e) {
              var deleteID = record.ID;
              deleteItem(deleteID);
              list.removeChild(listItem);
              make.focus();
            };
          }

          function handleSubmit(event) {
            if (this.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
              return;
            }
            this.classList.remove('was-validated');
            
            var record = { "make": make.value, "model": model.value, "year": year.value, "mileage": mileage.value };
            addItem( record );
            
            
            
            var formData = new FormData();
            formData.append("make", make.value );
            formData.append("model", model.value );
            formData.append("year", year.value );
            formData.append("mileage", mileage.value );

            var request = new XMLHttpRequest();
            request.open("POST", "shoppinglist.php");
            request.send(formData);

            make.value = '';
            model.value = '';
            year.value = '1';
            mileage.value = '1';
            make.focus();
            event.preventDefault();
            event.stopPropagation();
            form.classList.remove('was-validated');
          }
          document.getElementById('form1').addEventListener('submit', handleSubmit );
          
          
          function reqListener () {
            this.response.forEach( function( record ){
              console.log( record );
              addItem( record );
            });
          }
        

          var oReq = new XMLHttpRequest();
          oReq.addEventListener("load", reqListener);
          oReq.responseType = "json";
          oReq.open("GET", "shoppinglist.php");
          oReq.send();
          
          function deleteItem(deleteID){
            
            var drequest = new XMLHttpRequest();
            drequest.open("DELETE", "shoppinglist.php?ID=" + deleteID);
            drequest.send();
          }
          
          
          
        }, false);//2
      }());//1    
    </script>  
  </body>
</html>