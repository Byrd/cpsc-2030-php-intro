<?php
    header( "Content-type: application/json");
    
    $link = mysqli_connect( 'localhost', 'root', '66120sphinx' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    $dbName = "car";
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    switch ( $_SERVER['REQUEST_METHOD']) {
        
        case 'GET':
            $results = mysqli_query( $link, 'select * from cars' );
            
            if ( ! $results ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $shoppinglist = array();
                while( $record = mysqli_fetch_assoc( $results ) ) {
                    $shoppinglist[] = $record;
                }
                mysqli_free_result( $results );
                echo json_encode( $shoppinglist );
            }
            break;
        
        case 'POST':
            $safe_make = mysqli_real_escape_string( $link, $_REQUEST["make"] );
            $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
            $safe_year = $_REQUEST["year"] + 0;
            $safe_mileage = $_REQUEST["mileage"] + 0;
            if ( strlen( $safe_make ) <= 0 ||
                 strlen( $safe_make ) > 80 ||
                 strlen( $safe_model ) <= 0 ||
                 strlen( $safe_model ) > 80 ||
                 $safe_year <= 0 ||
                 $safe_mileage <= 0 ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }

            $query = "INSERT INTO cars ( make, model,year,mileage ) VALUES ( '$safe_make', '$safe_model', '$safe_year', '$safe_mileage' )";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;
        
        case 'DELETE':
            $deleteID = $_REQUEST['ID'];
            $deleteQuery = " DELETE FROM cars WHERE `ID` = $deleteID ";
            if ( ! mysqli_query( $link, $deleteQuery ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;

    }

?>