<!doctype html>

<?php
session_start();
if(!isset($_SESSION['loggedin'])){
$_SESSION['loggedin'] = false; }
?>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Cars - CPSC 2030</title>
  </head>
  <body class="container">
    <h1>Cars - CPSC 2030</h1>
    
    <?php
        if($_SESSION['loggedin']){
          print"<p style=\"color:#FF0000\";>you are login now</p>";
        }
    ?>

    <form class="needs-validation" novalidate method="POST" action="">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="make">Make</label>
            <input type="text" class="form-control" id="make" placeholder="" value="" required name="make">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder="" value="" required name="model">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="year">Year</label>
            <input type="number" class="form-control" id="year" placeholder="" value="" required name="year">
            <div class="invalid-feedback">
              Valid year is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="mileage">Mileage</label>
            <input type="number" class="form-control" id="mileage" placeholder="" value="" required name="mileage">
            <div class="invalid-feedback">
              Valid mileage is required.
            </div>
          </div>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit" id="add">Add car</button>
        <br>
        
        <?php
        if($_SESSION['loggedin']){
          echo "<a href=\"logout.php\"> LOGOUT </a>";}
        else{
          echo "<a href=\"login.php\"> LOGIN </a>";}
        ?>
        
        <br>
        <p>* you will not able to add a car without login status</p>
        <p>* after finish, please log out</p>
    </form>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Make</th>
                <th scope="col">Model</th>
                <th scope="col">Year</th>
                <th scope="col">Mileage</th>
            </tr>
        </thead>
        <tbody id="result">
            
        <?php
            $link = mysqli_connect( 'localhost', 'root', '66120sphinx' );
            if (mysqli_connect_errno($link)) {  
              echo "Fail to Connect: " . mysqli_connect_error();  
              
            }  

            mysqli_select_db( $link, 'car' );
            if(!mysqli_select_db( $link, 'car' )){
              echo "Error on select db!";
            }
            
            
            if (isset( $_REQUEST["make"] )){//4
            
            if($_SESSION['loggedin']){//3

              $safe_make = mysqli_real_escape_string( $link, $_REQUEST["make"] );
              $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
              $safe_year = mysqli_real_escape_string( $link, $_REQUEST["year"] );
              $safe_mileage = mysqli_real_escape_string( $link, $_REQUEST["mileage"] );
           
              $check1=preg_match("/[a-zA-Z_]/", $safe_make);
              $check2=preg_match("/[a-zA-Z0-9_]/", $safe_model);
              $check3=preg_match("/[0-9]/", $safe_year);
              $check4=preg_match("/[0-9]/", $safe_mileage);
            
              
          if($check1==1&&$check2==1&&$check3==1&&$check4==1){//2
  
              $query = "INSERT INTO cars ( make, model,year,mileage ) VALUES ( '$safe_make', '$safe_model', '$safe_year', '$safe_mileage' )";

                    if (! mysqli_query( $link, $query )) {//1
                      print"<br>";
                      print"error from mysql:<br>";
                      print_r(mysqli_error_list($link)); }//1
            
          }//2
                      
            else{//2
              echo "<div class=\"alert alert-warning\">
	                  <a href=\"#\" class=\"close\" data-dismiss=\"alert\">
		                &times;</a><strong>Warming！</strong>Please fill the valid data
                    </div>";}//2
              
            }//3
              
            else{//3
              echo "<script type='text/javascript'>alert('please login first!');</script>";
              
            }//3
              
            }//4
          
            
            
            $results = mysqli_query( $link, 'SELECT * FROM cars' );
            if (! $results ) { 
                  print_r(mysqli_error_list($link)); }
            
            while( $record = mysqli_fetch_assoc( $results ) ) {
                $ID = $record['ID'];
                $make = $record['make'];
                $model = $record['model'];
                $year = $record['year'];
                $mileage = $record['mileage'];
                print "<tr><td>$ID</td><td>$make</td><td>$model</td><td>$year</td><td>$mileage</td><td>
                <form class=\"needs-validation\" novalidate method=\"POST\" action=\"\">
                <input type=\"hidden\" value=\"delete\" id=\"delete\" name=\"delete\">
                <input type=\"hidden\" value=\"$ID\" id=\"id2delete\" name=\"id2delete\">
                <button type=\"submit\" class=\"btn btn-danger\">Delete</button></form></td></tr>";
            }
            
            if($_SESSION['loggedin']){
            
            if ( isset( $_REQUEST["delete"] ) ) {
                $query = "DELETE FROM `cars` WHERE ID=" . $_REQUEST["id2delete"];
                if (! mysqli_query( $link, $query )) { 
                    print_r(mysqli_error_list($link)); }
                header("Refresh:0");}}
            
            
            mysqli_free_result( $results );
            if(! mysqli_close( $link )){
              echo "Error on close db!";
            }
            ?>

        </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
   
  </body>
</html>
