<!DOCTYPE html>

<?php
session_start();
if(!isset($_SESSION['loggedin'])){
$_SESSION['loggedin'] = false; }
if(!isset($_SESSION['userName'])){
$_SESSION['userName'] = "GUEST"; }
if(!isset($_SESSION['userEmail'])){
$_SESSION['userName'] = ""; }
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Gacha Game Recorder</title>
    <link rel="stylesheet" href="common.css">

    <!-- boostrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="form-validation.js"></script>
  </head>
  <body>
    <header>
      
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><?php echo "Hello " . $_SESSION['userName'] ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Index <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="intro.php">Intro</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documentation.php">Documentation</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      
      <?php
        if($_SESSION['loggedin']){
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" onclick=\"window.location.href='logout.php'\" >Logout</button>";}
        else{
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\"  style=\"margin: 10px\" onclick=\"window.location.href='register.php'\">Register</button>";
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" onclick=\"window.location.href='login.php'\">Login</button>";
        }
        ?>
        
    </form>
  </div>
</nav>


    </header>
    
    <main class="container">
      <p> </p> <br><br>
      <div class="row h-100">
        <div class="col-sm-12 my-auto text-center">
            
            
<div class="row">
  <div class="col-sm-4 text-left">
      <h3>Register Page</h3>
      <p>A simple register page.</p>
      <p>Users fill their info, and the app will store the info in to database using AJAX</p>
      <P>If a user fills anything wrong, there will be some text to remind the user.</P>
      <P>After regiser succeed, session will record the user's name and make a greeting.</P></div>
  <div class="col-sm-8">
      
      <div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Default Page
        </button>
      </h5>
    </div>
    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
          <img src="register-1.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Invalid Email
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
       <img src="register-2.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Wrong Password
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <img src="register-3.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
   <div class="card">
    <div class="card-header" id="heading5">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
          Register Succeed 
        </button>
      </h5>
    </div>
    <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
      <div class="card-body">
       <img src="register-4.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
   <div class="card">
    <div class="card-header" id="headingFour">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          greeting in nav bar will show the user name
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
       <img src="register-5.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
</div>
      
      
  </div>
</div>
  <p> </p>
<div class="row">
    
      <div class="col-sm-4  text-left">
      <h3>Login Page</h3>
      <p>A simple login page.</p>
      <p>Users fill their info, and the app will go to the database to check if the password is match with the email using AJAX.</p>
      <P>If a user fills anything wrong, there will be some text to remind the user.</P>
      <P>After login succeed, session will record the user's name and make a greeting.</P></div>
  
  <div class="col-sm-8">
      
            <div id="accordion1">
  <div class="card">
    <div class="card-header" id="headingOne1">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
          Default Page
        </button>
      </h5>
    </div>
    <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne1" data-parent="#accordion1">
      <div class="card-body">
          <img src="login-1.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo1">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo1">
          Wrong Password
        </button>
      </h5>
    </div>
    <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo1" data-parent="#accordion1">
      <div class="card-body">
       <img src="login-2.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree1">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="false" aria-controls="collapseThree1">
          Login Succeed
        </button>
      </h5>
    </div>
    <div id="collapseThree1" class="collapse" aria-labelledby="headingThree1" data-parent="#accordion1">
      <div class="card-body">
        <img src="login-3.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>

</div>
      
      </div>

</div>
  <p> </p>
<div class="row">
       <div class="col-sm-4  text-left">
      <h3>Index Page</h3>
      <p>You can try to login with as follow:</p>
      <p>account:<b>333@mail.com</b></p>
      <p>password:<b>123</b></p>
      <p>or rigister your own account.</p>
      <p>Fuctional Page. Before Logined , users can do nothing but see a line to remind them to register or to login.</p>
      <p>After registered or logined, the Users fill their data into the form, after clicked the update data button,the app will use AJAX to instore their data.</p>
      <P>The data should be filled correctly as the instruction below the submit button.</P>
      <P>A pie chart show how many ssr you have.</P>
      <P>Comment is automatically generated after compared the data users fill in.</P>
      <P>Click delete button to delete a record.</P></div>
      
  <div class="col-sm-8">
      
               <div id="accordion2">
  <div class="card">
    <div class="card-header" id="headingOne2">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
          Logout index
        </button>
      </h5>
    </div>
    <div id="collapseOne2" class="collapse show" aria-labelledby="headingOne2" data-parent="#accordion2">
      <div class="card-body">
          <img src="index-2.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo2">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
          login index
        </button>
      </h5>
    </div>
    <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo2" data-parent="#accordion2">
      <div class="card-body">
       <img src="index-1.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
  
      
      
      
      </div>
  
  
</div>
<p></p>
<hr>
<div class="row">
  <div class="col  text-left">
      <h5>Something I think COOL</h5>
      <p>Some Function I think cool in the site.</p>
      </div>
  <div class="col"><div class="card" style="width: 18rem;">
  <img class="card-img-top" src="p1.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Pie Chart by fusion chart</h5>
    <p class="card-text">get data from the database and generated a chart.</p>
    <a href="index.php" class="btn btn-primary">Go</a>
  </div>
</div></div>
  <div class="col"><div class="card" style="width: 18rem;">
  <img class="card-img-top" src="p2.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Carousel</h5>
    <p class="card-text">A slideshow component for cycling through elements—images or slides of text—like a carousel.</p>
    <a href="intro.php" class="btn btn-primary">Go</a>
  </div>
</div></div>
  <div class="col"><div class="card" style="width: 18rem;">
  <img class="card-img-top" src="p3.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Accordion</h5>
    <p class="card-text">Using the card component, you can extend the default collapse behavior to create an accordion.</p>
    <a href="documentation.php" class="btn btn-primary">Go</a>
  </div>
</div></div>
</div>


  </div>
  
  <p> </p> <br><br>
  </div>


    </main>


  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 text-light bg-dark">© 2019 Copyright:
    <a href="https://byrdeath.mooo.com"> Yaopeng Wu</a>
  </div>
  <!-- Copyright -->