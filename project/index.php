<!DOCTYPE html>

<?php
session_start();
if(!isset($_SESSION['loggedin'])){
$_SESSION['loggedin'] = false; }
if(!isset($_SESSION['userName'])){
$_SESSION['userName'] = "GUEST"; }
if(!isset($_SESSION['userEmail'])){
$_SESSION['userName'] = ""; }
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Gacha Game Recorder</title>
    <link rel="stylesheet" href="common.css">
    <link rel="stylesheet" href="grid-layout.css">
    
    <!-- Include fusioncharts core library file -->
        <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
        <!-- Include fusion theme file -->
        <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
        <!-- Include fusioncharts jquery plugin -->
        <script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>  
    
    <!-- boostrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="form-validation.js"></script>
  </head>
  <body>
    <header>
      
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><?php echo "Hello " . $_SESSION['userName'] ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Index <span class="sr-only">(current)</span></a>
      </li>
           <li class="nav-item">
        <a class="nav-link" href="intro.php">Intro</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documentation.php">Documentation</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      
      <?php
        if(($_SESSION['loggedin'])){
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" onclick=\"window.location.href='logout.php'\" >Logout</button>";}
        else{
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\"  style=\"margin: 10px\" onclick=\"window.location.href='register.php'\">Register</button>";
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" onclick=\"window.location.href='login.php'\">Login</button>";
        }
        ?>
    </form>
  </div>
</nav>


    </header>
    
    <main>
      
      <div class="row h-100">
        <div class="col-sm-12 my-auto text-center">
        <div class="container">
          
          <div class="row">
            <div class="col">
               <p> </p> <br><br>
             <form id="form1">
             
               <div class="form-group text-left">
                 <label for="eventName">Event Name</label>
                 <input type="text" class="form-control" id="eventName" placeholder="Some Event Name" required>
               </div>
               
              <div class="form-row">
               
               <div class="form-group col-md-4 text-left">
                 <label for="payPull">Pay Pull</label>
                 <input type="number" class="form-control" id="payPull" placeholder="0">
               </div>
               
                <div class="form-group col-md-4 text-left">
                 <label for="freePull">Free Pull</label>
                 <input type="number" class="form-control" id="freePull" placeholder="0">
               </div>
               
                <div class="form-group col-md-4 text-left">
                 <label for="officialSSRratio">Official SSR Ratio</label>
                 <input type="number" class="form-control" id="officialSSRratio" step="0.01" placeholder="0">
                </div>
                
                </div>
                
                <div class="form-row">
                  
                <div class="form-group col-md-3 text-left">
                 <label for="ssrNumber">SSR Number</label>
                 <input type="number" class="form-control" id="ssrNumber" placeholder="0">
                </div>
               
                <div class="form-group col-md-3 text-left">
                 <label for="srNumber">SR Number</label>
                 <input type="number" class="form-control" id="srNumber" placeholder="0">
                </div>
               
                <div class="form-group col-md-3 text-left">
                 <label for="rNumber">R Number</label>
                 <input type="number" class="form-control" id="rNumber" placeholder="0">
                </div>
                
                <div class="form-group col-md-3 text-left">
                 <label for="bonusSSR">Bonus SSR</label>
                 <input type="number" class="form-control" id="bonusSSR" placeholder="0">
                </div>
                  
                </div>
                
                <button type="submit" class="btn btn-primary">Update Data</button>
                <br><br><small class="d-flex justify-content-between text-warning">**Pay Pull + Free Pull should never less or equal to 0</small>
                <small class="d-flex justify-content-between text-warning">**Every Number type data should never less than 0</small>
                <small class="d-flex justify-content-between text-warning">**Pay Pull + Free Pull should always equal to SSR + SR + R</small><br><br><p> </p>
               
             </form>
              </div>
          <div class="col">
            <p> </p> <br><br>
             <?php if($_SESSION['loggedin']){ 
               echo " <div id=\"chart-container\"></div> ";} ?>
               <?php if(!$_SESSION['loggedin']){
             echo " <div class=\"col\"><a href=\"register.php\">Register</a> or <a href=\"login.php\">Login</a> to use the Function!</div>";}?>
              </div>
              </div>
              
 
             <?php if($_SESSION['loggedin']){ 
               echo "
              <ul class=\"list-group\" name=\"group\"></ul>
              
              <table class=\"table\">
                <thead>
            <tr>
                <th scope=\"col\">Event Name</th>
                <th scope=\"col\">Pay Pull</th>
                <th scope=\"col\">Free Pull</th>
                <th scope=\"col\" >SSR</th>
                <th scope=\"col\" >SR</th>
                <th scope=\"col\" >R</th>
                <th scope=\"col\" >Bonus SSR</th>
                <th scope=\"col\" >Official SSR Ratio</th>
                <th scope=\"col\" >Your SSR Ratio</th>
                <th scope=\"col\" >Comment</th>
                <th scope=\"col\">Delete</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>"
            
            ;}?>
            

        </div>
        </div>
        </div>
      


    </main>


  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 text-light bg-dark">© 2019 Copyright:
    <a href="https://byrdeath.mooo.com"> Yaopeng Wu</a>
  </div>
  <!-- Copyright -->


    <script src="form-validation.js"></script>

    
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function () {//1
        'use strict';

        window.addEventListener('load', function () {//2
          var list = document.querySelector('tbody');
          var form = document.querySelector('form');
          var eventName = document.querySelector('#eventName');
          eventName.focus();
          var payPull = document.querySelector('#payPull');
          var freePull = document.querySelector('#freePull');
          var officialSSRratio = document.querySelector('#officialSSRratio');
          var ssrNumber = document.querySelector('#ssrNumber');
          var srNumber = document.querySelector('#srNumber');
          var rNumber = document.querySelector('#rNumber');
          var bonusSSR = document.querySelector('#bonusSSR');
          var comment="";
          var yourRatio="";
          
          var totalSSRnumber=0;
          var totalSRnumber=0;
          var totalRnumber=0;
          var totalBonusSSRnumber=0;
          var data123 = [];


          function addItem( record ) {
            
            var listItem = document.createElement('tr');
            var listItemChildren1 = document.createElement('td'); 
            var listItemChildren2 = document.createElement('td'); 
            var listItemChildren3 = document.createElement('td'); 
            var listItemChildren4 = document.createElement('td'); 
            var listItemChildren5 = document.createElement('td');
            var listItemChildren6 = document.createElement('td'); 
            var listItemChildren7 = document.createElement('td'); 
            var listItemChildren8 = document.createElement('td'); 
            var listItemChildren9 = document.createElement('td'); 
            var listItemChildren10 = document.createElement('td'); 
            var listItemChildren11 = document.createElement('td'); 
            var listText = document.createElement('span');
            var listBtn = document.createElement('button');
            
             listBtn.setAttribute( "class", "btn btn-danger");
        
        listItem.appendChild(listItemChildren1);
        listItemChildren1.textContent = record.eventName ; 
        listItem.appendChild(listItemChildren2);
        listItemChildren2.textContent = record.payPull  ; 
        listItem.appendChild(listItemChildren3);
        listItemChildren3.textContent = record.freePull ; 
        listItem.appendChild(listItemChildren4);
        listItemChildren4.textContent = record.ssr;
        listItem.appendChild(listItemChildren5);
        listItemChildren5.textContent = record.sr ; 
        listItem.appendChild(listItemChildren6);
        listItemChildren6.textContent = record.r ; 
        listItem.appendChild(listItemChildren7);
        listItemChildren7.textContent =  record.bonusSSR ; 
        listItem.appendChild(listItemChildren8);
        listItemChildren8.textContent = record.officialSSRratio ; 
        listItem.appendChild(listItemChildren9);
        listItemChildren9.textContent = record.yourRatio ; 
        listItem.appendChild(listItemChildren10);
        listItemChildren10.textContent = record.comment ; 
        listItem.appendChild(listItemChildren11);
        listItemChildren11.appendChild(listBtn);
        listBtn.textContent = 'Delete';
        listBtn.setAttribute("class", "btn btn-danger");
        list.appendChild(listItem);
        listBtn.onclick = function(e) {
          var deleteID = record.id;
          deleteItem(deleteID);
          totalSSRnumber =Number( totalSSRnumber - (Number(record.ssr) + 0));
          totalSRnumber = Number( totalSRnumber - (Number(record.sr) + 0));
          totalRnumber = Number( totalRnumber - (Number(record.r) + 0));
          totalBonusSSRnumber = Number( totalBonusSSRnumber - (Number(record.bonusSSR) + 0));
          makePiechart();
          list.removeChild(listItem);
          eventName.focus();
        }
            
            
          }

          function handleSubmit(event) {
            
            

          
            
          totalSSRnumber=0;
          totalSRnumber=0;
          totalRnumber=0;
          totalBonusSSRnumber=0;
           
          yourRatio = ssrNumber.value / (parseFloat(payPull.value) + parseFloat(freePull.value)) ; 
          
          if(parseFloat(officialSSRratio.value)==yourRatio){comment="you are OK";}
          else if(parseFloat(officialSSRratio.value)<yourRatio){comment="you are lucky";}
          else{comment="you are not lucky enought";}
            
            
            if (this.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
              return;
            }
            this.classList.remove('was-validated');
            
            
            var record = { "eventName": eventName.value, "payPull": payPull.value, "freePull": freePull.value , "ssr": ssrNumber.value, "sr": srNumber.value , "r": rNumber.value, "bonusSSR": bonusSSR.value, "officialSSRratio": officialSSRratio.value , "yourRatio": yourRatio.value, "comment": comment.value };
            addItem( record );
            
            
            var formData = new FormData();
            formData.append("eventName", eventName.value );
            formData.append("payPull", payPull.value );
            formData.append("freePull", freePull.value );
            formData.append("ssr", ssrNumber.value );
            formData.append("sr", srNumber.value );
            formData.append("r", rNumber.value );
            formData.append("bonusSSR", bonusSSR.value );
            formData.append("officialSSRratio", officialSSRratio.value );
            formData.append("yourRatio", yourRatio);
            formData.append("comment", comment);
            

            var request = new XMLHttpRequest();
            request.open("POST", "user.php");
            request.send(formData);

            eventName.value = '';
            payPull.value = '0';
            freePull.value = '0';
            ssrNumber.value = '0';
            srNumber.value = '0';
            rNumber.value = '0';
            bonusSSR.value = '0';
            officialSSRratio.value = '0.01';
            yourRatio.value = '0';
            comment.value = '';
            
            eventName.focus();
            event.preventDefault();
            event.stopPropagation();
            form.classList.remove('was-validated');
          }
          document.getElementById('form1').addEventListener('submit', handleSubmit );
          
          
          function reqListener () {
            this.response.forEach( function( record ){
              addItem( record );
              totalSSRnumber =Number( totalSSRnumber + (Number(record.ssr) + 0));
              totalSRnumber = Number( totalSRnumber + (Number(record.sr) + 0));
              totalRnumber = Number( totalRnumber + (Number(record.r) + 0));
              totalBonusSSRnumber = Number( totalBonusSSRnumber + (Number(record.bonusSSR) + 0));
            });
          data123.push({label: "SSR", value:totalSSRnumber});
          data123.push({label: "Bonus SSR", value:totalBonusSSRnumber});
          data123.push({label: "SR", value:totalSRnumber});
          data123.push({label: "R", value:totalRnumber});
          makePiechart();          
          
          }
        

          var oReq = new XMLHttpRequest();
          oReq.addEventListener("load", reqListener);
          oReq.responseType = "json";
          oReq.open("GET", "user.php");
          oReq.send();
          
          function deleteItem(deleteID){
            
            var drequest = new XMLHttpRequest();
            drequest.open("DELETE", "user.php?ID=" + deleteID);
            drequest.send();
          }
          
function makePiechart(){
  const dataSource = {
  chart: {
    caption: "what you own",
    plottooltext: "<b>$percentValue</b> of web servers run on $label servers",
    showlegend: "1",
    showpercentvalues: "1",
    legendposition: "bottom",
    usedataplotcolorforlabels: "1",
    theme: "fusion"
  },
  data:data123
};

FusionCharts.ready(function() {
  var myChart = new FusionCharts({
    type: "pie2d",
    renderAt: "chart-container",
    width: "100%",
    height: "100%",
    dataFormat: "json",
    dataSource
  }).render();
});
}

    
          

          
        }, false);//2
      }());//1    
    </script>  
  </body>
</html>

