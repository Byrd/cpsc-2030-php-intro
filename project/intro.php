<!DOCTYPE html>

<?php
session_start();
if(!isset($_SESSION['loggedin'])){
$_SESSION['loggedin'] = false; }
if(!isset($_SESSION['userName'])){
$_SESSION['userName'] = "GUEST"; }
if(!isset($_SESSION['userEmail'])){
$_SESSION['userName'] = ""; }
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Gacha Game Recorder</title>
    <link rel="stylesheet" href="common.css">

    <!-- boostrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="form-validation.js"></script>
  </head>
  <body>
    <header>
      
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><?php echo "Hello " . $_SESSION['userName'] ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Index <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="intro.php">Intro</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documentation.php">Documentation</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      
      <?php
        if($_SESSION['loggedin']){
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" onclick=\"window.location.href='logout.php'\" >Logout</button>";}
        else{
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\"  style=\"margin: 10px\" onclick=\"window.location.href='register.php'\">Register</button>";
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" onclick=\"window.location.href='login.php'\">Login</button>";
        }
        ?>
        
    </form>
  </div>
</nav>


    </header>
    
    <main class="container">
      <p> </p> <br><br>
      <div class="row h-100">
        <div class="col-sm-12 my-auto text-center">
            
  <div class="row bg-dark ">
  <div class="col-8">

<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="intro-1.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="intro-2.png" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="intro-3.jpg" alt="Third slide">
    </div>
  </div>
</div>

      
      
  </div>
  <div class="col-4">
      
      <h5 class="text-white text-left">What is Gocha Game?</h5>
      <p class="text-white text-left">Gacha games are video games that adapt and virtualize the gacha (capsule-toy vending machine) mechanic. 
      In the monetization of video games it is similar to loot box, in inducing players to spend money.
      Most of these games are free-to-play[1] mobile games. In gacha games, players spend virtual currency, which can be from a machine; 
      however real money is usually eventually spent to obtain the virtual currency and opportunities to use it.<br>
      The gacha game model began to be widely used in the early 2010s, faring particularly well in Japan.
      Almost all of the highest-grossing mobile games in Japan use it, and it has become an integral part of Japanese mobile game culture.</p>
      <div class="text-left">
      <small class="text-warning text-left">* first slide from:<a href="">click here</a></small><br>
      <small class="text-warning text-left">* second slide from:<a href="">click here</a></small><br>
      <small class="text-warning text-left">* third slide from:<a href="">click here</a></small></div>
      
      
  </div>

  </div>
  
  <br>
  
    <div class="row">
        
        
  <div class="col-4">
      
  <div class="card" style="width: 18rem;">
  <img class="card-img-top" src="intro-4.jpg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Types</h5>
    <p class="card-text">There many types for a gocha game!</p>
    <a href="https://twitter.com/hellabrett/status/730233817907109888" class="btn btn-primary">Click to the source</a>
  </div>
</div>
      
      
  </div>        
        
        
  <div class="col-8">

<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Complete gacha
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        "Complete gacha" (コンプリートガチャ), also shortened as "kompu gacha" or "compu gacha" (コンプガチャ), 
        was a monetization model popular in Japanese mobile phone video games until 2012, 
        when it was rendered illegal via legal opinion. Under complete gacha rules, 
        players attempt to "complete" a set of common items in a particular loot pool in order to combine them into a rarer item.
        The first few items in a set can be rapidly acquired but as the number of missing items decreases it becomes 
        increasingly unlikely that redeeming a loot box will complete the set. 
        This is particularly true if there are a large number of common items in the game, since eventually one single, specific item is required.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Box gacha
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        Box gacha is a virtual box of set items with known probabilities. Its popularity grew around the time that the complete gacha controversy was becoming publicized. Box gacha is generally considered more fair because as items are pulled from the box, the likelihood of receiving the desired item increases, as there are fewer items in the box.[12] It is also possible to pull every item in the box, provided the player is willing to spend enough. For this reason, some players will calculate how much money it would take to ensure they pull the item of their choosing.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Sugoroku gacha
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        Sugoroku gacha is similar to that of a board game. Gacha mechanisms are used for the player to progress across the board, with further progress entailing better rewards. Each time that the player rolls the gacha, their character will either roll a dice or spin a wheel to determine how far the character will go.
      </div>
    </div>
  </div>
</div>

      
      
  </div>


  </div>
  
  <p> </p> <br><br>
  </div>


    </main>


  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 text-light bg-dark">© 2019 Copyright:
    <a href="https://byrdeath.mooo.com"> Yaopeng Wu</a>
  </div>
  <!-- Copyright -->
  

