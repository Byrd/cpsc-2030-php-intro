<?php
    header( "Content-type: application/json");
    session_start();
    
    $link = mysqli_connect( 'localhost', 'root', '66120sphinx' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    $dbName = "userData";
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }

    
    switch ( $_SERVER['REQUEST_METHOD']) {
    
        
        case 'POST':
            $safe_password = mysqli_real_escape_string( $link, $_REQUEST["pass"] );
            $safe_email = mysqli_real_escape_string( $link, $_REQUEST["useremail"] );
            
            if ( strlen( $safe_password ) <= 0 ||
                 strlen( $safe_password ) > 80 ||
                 strlen( $safe_email ) <= 0 ||
                 strlen( $safe_email ) > 80  ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }
            
            $results = mysqli_query( $link, "SELECT *  FROM userAccount WHERE userPassword = '$safe_password' AND userEmail = '$safe_email'");
            $record = mysqli_fetch_assoc( $results );
            if($record){
              $_SESSION['loggedin'] = true;
              $_SESSION['userName'] = $record["userName"];
              $_SESSION['userEmail'] = $safe_email;
            }
            mysqli_free_result( $results );
            echo json_encode( $record );
        

            $query = "INSERT INTO userAccount ( userName, userPassword,userEmail ) VALUES ( '$safe_username', '$safe_password', '$safe_email')";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;

    }

?>