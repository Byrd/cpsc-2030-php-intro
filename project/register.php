<!DOCTYPE html>

<?php
session_start();
if(!isset($_SESSION['loggedin'])){
$_SESSION['loggedin'] = false; }
if(!isset($_SESSION['userName'])){
$_SESSION['userName'] = "GUEST"; }
if(!isset($_SESSION['userEmail'])){
$_SESSION['userName'] = ""; }
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Gacha Game Recorder</title>
    <link rel="stylesheet" href="common.css">

    <!-- boostrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="form-validation.js"></script>
  </head>
  <body>
    <header>
      
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><?php echo "Hello " . $_SESSION['userName'] ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Index <span class="sr-only">(current)</span></a>
      </li>
           <li class="nav-item">
        <a class="nav-link" href="intro.php">Intro</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documentation.php">Documentation</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      
      <?php
        if($_SESSION['loggedin']){
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" onclick=\"window.location.href='logout.php'\" >Logout</button>";}
        else{
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\"  style=\"margin: 10px\" onclick=\"window.location.href='register.php'\">Register</button>";
          echo " <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" onclick=\"window.location.href='login.php'\">Login</button>";
        }
        ?>
        
    </form>
  </div>
</nav>


    </header>
    
    <main class="container">
      <p> </p> <br><br>
      <div class="row h-100">
        <div class="col-sm-12 my-auto text-center">
      <div class="card card-block w-28 mx-auto" style="max-width:600px;" id="registerCard">
  <div class="card-body">
     <form >
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="useremail" aria-describedby="emailHelp" placeholder="Enter email" required>
    <small id="emailHelp" class="form-text text-danger">We'll never share your email with anyone else.</small>
  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">User name</label>
    <input type="text" class="form-control" id="username" placeholder="Username" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="inputPassword1" placeholder="Password" required>
  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">Confirmed Password</label>
    <input type="password" class="form-control" id="inputPassword2" placeholder="Confirmed Password" required>
    <small id="passwordV"  class="form-text  text-danger">The Two Passwords You Enter Should be the Same!</small>
  </div>
  

  <button type="button" class="btn btn-primary" id="formSButton">Submit</button>
</form>

</div>
  </div>
  <p> </p> <br><br>
  </div>


    </main>


  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 text-light bg-dark">© 2019 Copyright:
    <a href="https://byrdeath.mooo.com"> Yaopeng Wu</a>
  </div>
  <!-- Copyright -->
  



    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function () {//1
        'use strict';

        window.addEventListener('load', function () {//2
          
          $("#formSButton").click(function(){//3
          

            if(inputPassword1.value===inputPassword2.value){
              
            var formData = new FormData();
            formData.append("useremail", useremail.value );
            formData.append("username", username.value );
            formData.append("pass", inputPassword1.value );
            
           function reqListener () {
             
             var mailValidation=1;
             
             for (var key in this.response){
               mailValidation = this.response[key];}
               
              if(mailValidation==1){
                console.log("this is exits");
                $("#emailHelp").text("THE EMAIL ADDRESS YOU HAVE ENTERED IS ALREADY REGISTERED!");
                $("#passwordV").text("The Two Passwords You Enter Should be the Same!");
              }else{
                console.log("this is not exits");
                alert("Register Successed!");
               window.location.href='index.php';

              }
          }


            
            var oReq = new XMLHttpRequest();
            oReq.addEventListener("load", reqListener);
            oReq.responseType = "json";
            oReq.open("POST", "registerServer.php");
            oReq.send(formData);
            
            }else{
              
              
              $("#passwordV").text("PLEASE ENTER THE SAME PASSWORD AGAIN!");
              $("#emailHelp").text("We'll never share your email with anyone else.");
              
            }
              
          });//3
          

          
        }, false);//2
      }());//1    
    </script>  


  </body>
</html>

