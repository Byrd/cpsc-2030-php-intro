<?php
    header( "Content-type: application/json");
    session_start();
    
    $link = mysqli_connect( 'localhost', 'root', '66120sphinx' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    $dbName = "userData";
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    switch ( $_SERVER['REQUEST_METHOD']) {
        
        case 'GET':
            $email = $_SESSION['userEmail'];
            $results = mysqli_query( $link, " select * from testData WHERE userEmail = '$email' " );
            
            if ( ! $results ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $shoppinglist = array();
                while( $record = mysqli_fetch_assoc( $results ) ) {
                    $shoppinglist[] = $record;
                }
                mysqli_free_result( $results );
                echo json_encode( $shoppinglist );
            }
            break;
        
        case 'POST':
            $safe_eventName = mysqli_real_escape_string( $link, $_REQUEST["eventName"] );
            $safe_payPull = $_REQUEST["payPull"] + 0;
            $safe_freePull = $_REQUEST["freePull"] + 0;
            $safe_ssr = $_REQUEST["ssr"] + 0;
            $safe_sr = $_REQUEST["sr"] + 0;
            $safe_r = $_REQUEST["r"] + 0;
            $safe_bonusSSR = $_REQUEST["bonusSSR"] + 0;
            $safe_officialSSRratio = $_REQUEST["officialSSRratio"] + 0;
            $safe_yourRatio = $_REQUEST["yourRatio"] + 0;
            $safe_comment = mysqli_real_escape_string( $link, $_REQUEST["comment"] );
            $email = $_SESSION['userEmail'];
            /*if ( strlen( $safe_make ) <= 0 ||
                 strlen( $safe_make ) > 80 ||
                 strlen( $safe_model ) <= 0 ||
                 strlen( $safe_model ) > 80 ||
                 $safe_year <= 0 ||
                 $safe_mileage <= 0 ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }*/

            $query = "INSERT INTO testData ( userEmail, eventName, payPull, freePull, ssr, sr, r, bonusSSR, officialSSRratio, yourRatio, comment) VALUES ( '$email', '$safe_eventName', '$safe_payPull', '$safe_freePull', '$safe_ssr', '$safe_sr', '$safe_r', '$safe_bonusSSR', '$safe_officialSSRratio', '$safe_yourRatio', '$safe_comment')";
            
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;
        
        case 'DELETE':
            $deleteID = $_REQUEST['ID'];
            $deleteQuery = " DELETE FROM testData WHERE `ID` = $deleteID ";
            if ( ! mysqli_query( $link, $deleteQuery ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;

    }

?>